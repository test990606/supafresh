import { createClient } from "supabase";

// Access environment variables
const supabaseUrl = Deno.env.get("SUPABASE_URL");
const supabaseAnonKey = Deno.env.get("SUPABASE_KEY");

export const supaClient = createClient(supabaseUrl, supabaseAnonKey);
