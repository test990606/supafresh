import { supaClient } from "../lib/db.ts";


export default function SignIn() {
  const signInWithTwitter = () => {
    
      supaClient.auth.signInWithOAuth({
        provider: "twitter",
      
    }); 
  
  }
  return <button onClick={signInWithTwitter}>Sign In with Twitter</button>;
}
